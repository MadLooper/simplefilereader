import java.io.*;
import java.util.Scanner;

/**
 * Created by KW on 8/2/17.
 */
public class Main {

    public static void main(String[] args){
        File naszPlik = new File("test.txt");
//        try (/*InputStream inputStream = new FileInputStream(naszPlik);*/
//             BufferedReader reader = new BufferedReader(new FileReader(naszPlik))) {
//                         // obsługa pliku
//            String liniaZPliku = "";
//            do{
//                System.out.println(liniaZPliku);
//                liniaZPliku = reader.readLine();
//            }while (liniaZPliku != null);
//        }catch (FileNotFoundException fnfe){
//
//        }catch (IOException ioe){
//
//        }

        try {
            Scanner sc = new Scanner(naszPlik);
            String liniaZPliku ;
            while (sc.hasNextLine()){
                liniaZPliku = sc.nextLine();
                System.out.println(liniaZPliku);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
